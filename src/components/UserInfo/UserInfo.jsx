import React from "react";

function UserInfo({userInfo}) {
    return (
        <div className="userInfo-container">
            <div className="user-logo">
                <img src={userInfo.logo} alt={userInfo.login} />
            </div>
            <div className="user-info">
                <div className="user-login">{userInfo.login}</div>
                <div className="user-email">{userInfo.email}</div>
            </div>
        </div>
    );
}

export default UserInfo;