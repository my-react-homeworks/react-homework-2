import React from "react";
import Sidebar from "../Sidebar/Sidebar";

const data = {
    user: {
        login: "Animated Fred",
        email: "anomated@demo.com",
        logo: "../../icons/avatar.png",
    },
    controls: [
        {
            icon: "../../icons/search.svg",
            title: "Search..",
        },
        {
            icon: "../../icons/dashboard.svg",
            title: "Dashboard",
        },
        {
            icon: "../../icons/revenue.svg",
            title: "Revenue",
        },
        {
            icon: "../../icons/notification.svg",
            title: "Notifications",
        },
        {
            icon: "../../icons/analytics.svg",
            title: "Analytics",
        },
        {
            icon: "../../icons/inventory.svg",
            title: "Inventory",
        },
    ],
    options: [
        {
            icon: "../../icons/logout.svg",
            title: "Logout",
        },
        {
            icon: "../../icons/l_mode.svg",
            title: "Light mode",
            switch: true
        },
    ],
};

function App (){
    return(
        <div className="wrapper">
            <Sidebar data={data}/>
        </div>
    )
}

export default App;