import React from "react";

function Switch(){
    return(
        <div className="switch">
            <input type="checkbox" id="switch" />
            <label htmlFor="switch">
                <div className="switcher"></div>
            </label>
        </div>
    )
}

export default Switch;