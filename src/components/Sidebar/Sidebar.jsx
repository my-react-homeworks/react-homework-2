import React from "react";
import UserInfo from "../UserInfo/UserInfo";
import Menu from "../Menu/Menu";
import Options from "../Options/Options";

function Sidebar({ data }) {
    const {user, controls, options} = data;
    return (
        <div className="sidebar">
            <div className="control">
                <img src="/icons/arrow.svg" alt="" />
            </div>
            <UserInfo userInfo={user} />
            <Menu menuItems={controls} />
            <Options options={options} />
        </div>
    );
}

export default Sidebar;
