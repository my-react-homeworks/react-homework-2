import React from "react";
import Item from "../Item/Item";

function Options({options}){
    return (
        <ul className="options">
            {options.map(item => <Item icon={item.icon} title={item.title} key={item.title} switch={item.switch}/>)}
        </ul>
    );
}

export default Options;