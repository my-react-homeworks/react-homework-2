import React from "react";
import Item from "../Item/Item";

function Menu({menuItems}){
    return(
        <ul className="menu">
            {menuItems.map(item => <Item icon={item.icon} title={item.title} key={item.title}/>)}
        </ul>
    )
}

export default Menu;