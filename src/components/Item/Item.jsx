import React from "react";
import Switch from "../Switch/Switch";

function Item(props){
    return (
        <li className="item">
            <div className="icon">
                <img src={props.icon} alt={props.title} />
            </div>
            <div className="title">{props.title}</div>
            {props.switch ? <Switch/> : null}
        </li>
    );
}

export default Item;